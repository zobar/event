event
=====

```sh
go get bitbucket.org/zobar/event
```

Package `event` provides a minimal select/poll/epoll/kqueue-based event loop for
Go applications that require evented IO notifications. This is particularly
useful for code that interfaces with asynchronous C libraries, which often
assume an event loop is available.

This is an interface-only package, and has no dependencies of its own.
Subpackage `libevent` provides a simple implementation which depends on the
[libevent](http://libevent.org) C library.

There are a handful of other C libraries which also provide event loops, such as
[libev](http://software.schmorp.de/pkg/libev.html),
[libuv](https://github.com/joyent/libuv),
[CoreFoundation's CFRunLoop](https://developer.apple.com/library/mac/documentation/CoreFoundation/Reference/CFRunLoopRef/Reference/reference.html),
[glib's GMainLoop](https://developer.gnome.org/glib/stable/glib-The-Main-Event-Loop.html),
and
[Qt's QSocketNotifier](http://qt-project.org/doc/qt-5.0/qtcore/qsocketnotifier.html).
If you implement one of these backends, I strongly encourage you to make a pull
request.

Please see [GoDoc](http://godoc.org/bitbucket.org/zobar/event) for full
documentation.
