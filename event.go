// Package event provides a minimal select/poll/epoll/kqueue-based event loop
// for Go applications that require evented IO notifications. This is
// particularly useful for code that interfaces with asynchronous C libraries,
// which often assume an event loop is available.
//
// This is an interface-only package, and has no dependencies of its own.
// Subpackage libevent provides a simple implementation which depends on the
// libevent C library (http://libevent.org).
//
// There are a handful of other C libraries which also provide event loops, such
// as libev (http://software.schmorp.de/pkg/libev.html), libuv
// (https://github.com/joyent/libuv), CoreFoundation's CFRunLoop
// (https://developer.apple.com/library/mac/documentation/CoreFoundation/Reference/CFRunLoopRef/Reference/reference.html),
// glib's GMainLoop
// (https://developer.gnome.org/glib/stable/glib-The-Main-Event-Loop.html), and
// Qt's QSocketNotifier
// (http://qt-project.org/doc/qt-5.0/qtcore/qsocketnotifier.html). If you
// implement one of these backends, I strongly encourage you to make a pull
// request.
package event

import "time"

// FD represents an OS file descriptor.
type FD uintptr

// IOFlags are used to describe file statuses. They may be bitwise-ORed if more
// than one status applies.
type IOFlags int16

const (
	// Read indicates that a file or socket is readable.
	Read IOFlags = 1 << iota

	// Write indicates that a file or socket is writable.
	Write
)

// An IOSource monitors an open file or socket's readable and/or writable
// status. IOSources are "level-triggered," meaning they will fire each time
// through the event loop for as long as the condition holds. (It also means
// that an infinite loop is likely to result if your event handler does not
// either change the file's state or remove itself from the loop.)
type IOSource Source

// Loop manages a set of IOSources and TimerSources. This is the main entry
// point to the event package. How you obtain a Loop depends on what type of
// code you're writing:
//
// If you are writing a library, your best bet is to accept a Loop as a
// parameter to your top-level constructor. This will make your library more
// flexible, since it insulates your code from the implementation decisions of
// your users.
//
// If you are writing an application, subpackage libevent makes a simple, shared
// event loop available through libevent.MainLoop(). Other implementations based
// on different libraries may eventually become available.
type Loop interface {

	// IOSource() creates a new IOSource for monitoring an open file's readable
	// and/or writable status. The event source is not active until Add() is
	// called on it.
	IOSource(fd FD, flags IOFlags, callback func(source IOSource, flags IOFlags)) IOSource

	// TimerSource() creates a new TimerSource for emitting events repeatedly
	// after a given timeout. The event source is not active until Add() is
	// called on it.
	TimerSource(timeout time.Duration, callback func(source TimerSource)) TimerSource
}

// Source is a common interface implemented by all event sources.
type Source interface {

	// Add makes an event source active. Once an event source is active, it will
	// emit events until it is explicitly removed.
	Add() error

	// Remove is called to deactivate an event source. After an event source is
	// deactivated, it will no longer emit events. An event source can be added
	// again after it's been removed.
	Remove() error
}

// TimerSource is an event source which emits events repeatedly after a given
// timeout.
type TimerSource Source
