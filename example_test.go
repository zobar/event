package event_test

import (
	"fmt"
	"bitbucket.org/zobar/event"
	"bitbucket.org/zobar/event/libevent"
	"os"
	"syscall"
	"time"
)

func ExampleIOSource() {
	wait := make(chan bool)

	// Get a reference to the shared libevent event loop.
	loop := libevent.MainLoop()

	hi := loop.IOSource(event.FD(syscall.Stdin), event.Read, func(event.IOSource, event.IOFlags) {
		// At this point, we have to read the bytes out of the file descriptor.
		// Otherwise the file descriptor will remain readable, and we will
		// continue to get notified.
		buf := make([]byte, 256)
		os.Stdin.Read(buf)

		fmt.Println("Hello, world!")
	})

	// Events need to be explicitly added, or else they won't fire.
	hi.Add()

	// IOSources have no intrinsic timeout. If a timeout is desired, you must
	// create a separate TimerSource.
	bye := loop.TimerSource(2*time.Second, func(event.TimerSource) {
		fmt.Println("Goodbye.")
		close(wait)
	})
	bye.Add()

	fmt.Println("Press enter quickly for a friendly message.")
	<-wait

	// Not normally necessary, but we need to clean up for the other examples.
	hi.Remove()
	bye.Remove()

	// Output:
	// Press enter quickly for a friendly message.
	// Goodbye.
}

func ExampleTimerSource() {
	wait := make(chan bool)

	// Get a reference to the shared libevent event loop.
	loop := libevent.MainLoop()

	// Events repeat until they are removed.
	tick := loop.TimerSource(500*time.Millisecond, func(event.TimerSource) {
		fmt.Printf("tick... ")
	})

	// Events need to be explicitly added, or else they won't fire.
	tick.Add()

	boom := loop.TimerSource(2*time.Second, func(event.TimerSource) {
		fmt.Printf("BOOM!\n")
		close(wait)
	})
	boom.Add()

	<-wait

	// Not normally necessary, but we need to clean up for the other examples.
	tick.Remove()
	boom.Remove()

	// Output:
	// tick... tick... tick... tick... BOOM!
}
