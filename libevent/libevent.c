#include "_cgo_export.h"
#include "libevent.h"

void dispatchSourceCb(evutil_socket_t fd, short events, void* arg) {
	dispatchSource(fd, events, arg);
}

void durationToTimeval(int64_t duration, struct timeval* timeval) {
	timeval->tv_sec = duration / 1000000000;
	timeval->tv_usec = (duration % 1000000000) / 1000;
}
