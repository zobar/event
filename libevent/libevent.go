// Package libevent provides a simple event loop based on the libevent C library
// (http://libevent.org). Libevent is a mature, stable library with excellent
// cross-platform compatibility.
package libevent

// #cgo pkg-config: libevent_pthreads
// #include <event2/event.h>
// #include <event2/thread.h>
// #include "libevent.h"
import "C"

import (
	"bitbucket.org/zobar/event"
	"runtime"
	"time"
	"unsafe"
)

const maxTimeout = 100000000 * time.Second

var mainLoop event.Loop

// MainLoop provides convenient access to a shared event loop managed by
// libevent.
func MainLoop() event.Loop {
	return mainLoop
}

//export dispatchSource
func dispatchSource(fd uintptr, flags int16, arg unsafe.Pointer) {
	dispatcher := (*dispatcher)(arg)
	(*dispatcher).dispatch(flags)
}

func finalize(freer freer) {
	freer.free()
}

func fromIOFlags(in event.IOFlags) (out C.short) {
	if in&event.Read != 0 {
		out |= C.EV_READ
	}
	if in&event.Write != 0 {
		out |= C.EV_WRITE
	}
	return
}

func init() {
	C.evthread_use_pthreads()
	mainLoop = newLoop()
}

func newLoop() event.Loop {
	loop := &loop{ptr: C.event_base_new()}
	runtime.SetFinalizer(loop, finalize)
	loop.TimerSource(maxTimeout, nil).Add()
	go loop.run()
	return loop
}

func toIOFlags(in int16) (out event.IOFlags) {
	if in&C.EV_READ != 0 {
		out |= event.Read
	}
	if in&C.EV_WRITE != 0 {
		out |= event.Write
	}
	return
}

// Error indicates a generic libevent error. Unfortunately, libevent does not
// provide any detail about the errors that have occurred.
type Error struct{}

// Error returns the string "libevent encountered an unexpected error".
func (error *Error) Error() string {
	return "libevent encountered an unexpected error"
}

type dispatcher interface {
	dispatch(flags int16)
}

type freer interface {
	free()
}

type ioSource struct {
	*source
	cb func(event.IOSource, event.IOFlags)
}

func (source *ioSource) Add() error {
	return source.add(nil)
}

func (source *ioSource) dispatch(flags int16) {
	if source.cb != nil {
		source.cb(source, toIOFlags(flags))
	}
}

type loop struct{ ptr *C.struct_event_base }

func (loop *loop) IOSource(fd event.FD, flags event.IOFlags, callback func(event.IOSource, event.IOFlags)) event.IOSource {
	source := &ioSource{cb: callback}
	source.source = loop.newSource(C.int(fd), fromIOFlags(flags)|C.EV_PERSIST, source)
	return source
}

func (loop *loop) TimerSource(timeout time.Duration, callback func(event.TimerSource)) event.TimerSource {
	source := &timerSource{cb: callback}
	C.durationToTimeval(C.int64_t(timeout), &source.timeout)
	source.source = loop.newSource(-1, C.EV_PERSIST, source)
	return source
}

func (loop *loop) free() {
	C.event_base_free(loop.ptr)
}

func (loop *loop) newSource(fd C.int, flags C.short, dispatcher dispatcher) (out *source) {
	dispatcherPtr := &dispatcher
	out = &source{
		dispatcher: dispatcherPtr,
		ptr:        C.event_new(loop.ptr, C.int(fd), flags, (*[0]byte)(C.dispatchSourceCb), unsafe.Pointer(dispatcherPtr)),
	}
	runtime.SetFinalizer(out, finalize)
	return
}

func (loop *loop) run() {
	C.event_base_dispatch(loop.ptr)
}

type source struct {
	dispatcher *dispatcher
	ptr        *C.struct_event
}

func (source *source) Remove() (err error) {
	if C.event_del(source.ptr) != 0 {
		err = &Error{}
	}
	return
}

func (source *source) add(timeout *C.struct_timeval) (err error) {
	if C.event_add(source.ptr, timeout) != 0 {
		err = &Error{}
	}
	return
}

func (source *source) free() {
	C.event_free(source.ptr)
}

type timerSource struct {
	*source
	cb      func(event.TimerSource)
	timeout C.struct_timeval
}

func (source *timerSource) Add() error {
	return source.add(&source.timeout)
}

func (source *timerSource) dispatch(flags int16) {
	if source.cb != nil {
		source.cb(source)
	}
}
