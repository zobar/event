#include <event2/event.h>

void dispatchSourceCb(evutil_socket_t fd, short events, void* arg);

void durationToTimeval(int64_t duration, struct timeval* timeval);
